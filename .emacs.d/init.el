(org-babel-load-file (expand-file-name "config.org" user-emacs-directory))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(yasnippet-snippets yasnippet lsp-pyright lua-mode impatient-mode ccls tree-sitter-langs flycheck dap-mode lsp-ivy lsp-treemacs lsp-ui lsp-mode evil-multiedit evil-commentary smartparens company-box company treemacs-projectile treemacs-magit treemacs-icons-dired treemacs-evil treemacs smex vterm-toggle vterm org-auto-tangle toc-org visual-fill-column org-bullets magit peep-dired all-the-icons-dired dired-single dired-open dashboard hide-mode-line doom-modeline centaur-tabs solaire-mode doom-themes highlight-indent-guides rainbow-mode emojify all-the-icons-ivy-rich all-the-icons general sudo-edit counsel-projectile projectile which-key evil-tutor evil-collection evil gcmh use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
